import java.util.ArrayList;

public class bingo {
	public static void main(String[] args) {
		card Card = new card();

		// B列の準備
		ArrayList<Integer> bColumn = Card.getColumnNumber(1, 15);
		// I列の準備
		ArrayList<Integer> iColumn = Card.getColumnNumber(16, 30);
		// N列の準備
		ArrayList<Integer> nColumn = Card.getColumnNumber(31, 45);
		// G列の準備
		ArrayList<Integer> gColumn = Card.getColumnNumber(46, 60);
		// O列の準備
		ArrayList<Integer> oColumn = Card.getColumnNumber(61, 75);
		//抽選番号
		ArrayList<Integer> num1 = Card.getColumnNumber(1, 75);
		//ビンゴカードの型を作るクラス
		bingocard card = new bingocard();
		String[][] bingocard = card.bingocard();
		//ビンゴカードに数字を入れるクラス
		cardadd Cardadd = new cardadd();
		String[][] bingoCard = Cardadd.add(bColumn, iColumn, nColumn, gColumn, oColumn, bingocard);
		int ball = 0;
		for (Integer num : num1) {
			ball++;
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {

					String numString;
					if (num < 10) {
						numString = "0" + num.toString();
					} else {
						numString = num.toString();
					}

					if (bingoCard[i][j].equals(numString)) {
						bingoCard[i][j] = "(" + bingoCard[i][j] + ")";
					}
				}
			}
			//ビンゴの判定するクラス
			BingoJudge Bingo=new BingoJudge();
			int  bingonum  =Bingo.Bingo(bingoCard);
			//リーチ判定するクラス
			reachJudge reach=new reachJudge();
			int rearchNum =reach.reachJudge(bingoCard);

			System.out.println(ball + "回目" + "" + "抽選番号" + num + "番");
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					if (bingoCard[i][j].contains("(")) {
						System.out.print(bingoCard[i][j]);
					} else if (bingoCard[i][j].equals("FREE")) {
						System.out.print(bingoCard[i][j]);
					} else {
						System.out.print(" " + bingoCard[i][j] + " ");
					}
				}
				System.out.println();
			}

			System.out.println();
			System.out.println("リーチ数" + rearchNum + "です");
			System.out.println("ビンゴ数" + bingonum + "です");
			System.out.println("--------------------");
			if (bingonum == 12) {
				break;
			}
		}
	}

}
